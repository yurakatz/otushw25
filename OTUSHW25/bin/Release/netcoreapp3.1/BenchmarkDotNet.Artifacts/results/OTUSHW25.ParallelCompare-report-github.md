``` ini

BenchmarkDotNet=v0.12.1, OS=Windows 10.0.19042
AMD Ryzen 7 3700X, 1 CPU, 16 logical and 8 physical cores
.NET Core SDK=5.0.102
  [Host]     : .NET Core 3.1.11 (CoreCLR 4.700.20.56602, CoreFX 4.700.20.56604), X64 RyuJIT  [AttachedDebugger]
  DefaultJob : .NET Core 3.1.11 (CoreCLR 4.700.20.56602, CoreFX 4.700.20.56604), X64 RyuJIT


```
|         Method | ArraySize |         Mean |      Error |     StdDev | Ratio |
|--------------- |---------- |-------------:|-----------:|-----------:|------:|
|     **RegularSum** |    **100000** |    **358.84 μs** |   **3.690 μs** |   **3.271 μs** |  **1.00** |
|     AsParallel |    100000 |     83.89 μs |   0.663 μs |   0.621 μs |  0.23 |
| AsParallelTask |    100000 |    146.63 μs |   1.560 μs |   1.383 μs |  0.41 |
|                |           |              |            |            |       |
|     **RegularSum** |   **1000000** |  **3,593.98 μs** |  **20.951 μs** |  **17.495 μs** |  **1.00** |
|     AsParallel |   1000000 |    667.03 μs |   2.020 μs |   1.890 μs |  0.19 |
| AsParallelTask |   1000000 |  1,249.09 μs |  21.563 μs |  20.170 μs |  0.35 |
|                |           |              |            |            |       |
|     **RegularSum** |  **10000000** | **38,120.56 μs** | **178.975 μs** | **158.657 μs** |  **1.00** |
|     AsParallel |  10000000 |  5,081.59 μs |  93.474 μs |  82.862 μs |  0.13 |
| AsParallelTask |  10000000 | 11,723.71 μs | 226.260 μs | 222.218 μs |  0.31 |
