﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;

namespace OTUSHW25
{
    // [Config(typeof(ParallelCompareConfig))]
    public class ParallelCompare
    {
        private int[] _data;

        [Params(100_000, 1_000_000, 10_000_000)]
        public int ArraySize;

        [GlobalSetup]
        public void GlobalSetup()
        {
            var randNum = new Random();

            _data = Enumerable
                .Repeat(0, ArraySize)
                .Select(i => randNum.Next(-1000, 1000))
                .ToArray();
        }

        [Benchmark(Baseline = true)]
        public void RegularSum()
        {
            _data.Sum();
        }

        [Benchmark]
        public void AsParallel()
        {
            _data.AsParallel().Sum();
        }

        [Benchmark]
        public async Task<int> AsParallelTask()
        {
            var tasksList = new List<Task<int>>();

            var splited = _data.Split(_data.Length / Environment.ProcessorCount);

            foreach (var variable in splited) tasksList.Add(Task.Run(() => variable.Sum()));
            await Task.WhenAll();
            return tasksList.Sum(p => p.Result);
        }
    }
}